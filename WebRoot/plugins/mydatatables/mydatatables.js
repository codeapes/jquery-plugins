;(function($, window, document,undefined){
	"use strict";
	/**
	 * 
	 */
	$.fn.myDatatables = function(options,param){
		if (typeof options == 'string'){
			var method = $.fn.myDatatables.methods[options];
			if (method){
				/* 调用方法 */
				return method(this, param);
			}
		}
		options = options || {};
		return this.each(function(){
			/* 执行实例化 */
			if(!$.data(this, "datatable-api"))
				$.fn.myDatatables.methods.init(this,options);
		});
	}
	/**
	 * 版本信息
	 */
	$.fn.myDatatables.version = {
		number:'1.0.0.20170222'
	};
	/**
	 * 默认配置
	 */
	$.fn.myDatatables.defaults = {
		mapping:{
			ajax:{
				recordsTotal:'total',
				recordsFiltered:'total',
				data:'data'
			}
		}
	};
	/**
	 * 对外提供的方法
	 */
	$.fn.myDatatables.methods = {
		/**
		 * 插件初始化
		 * @param dom
		 * @param options
		 */
		init:function(dom,options){
        	/**
        	 * 设置 forms
        	 */
        	var forms = [];
        	if(options.form){
        		if(typeof options.form == 'string'){
        			forms.push($(options.form));
        		}
        		options.form = undefined;
        	}
        	
    		/**
    		 * 设置datatables api 至 dom 一个属性中
    		 */
    		var $dom = $(dom);
    		$dom.data('datatable-forms',forms);
    		$dom.data('datatable-api',$dom.dataTable($.extend(true,{
    			ordering:false,
    			autoWidth:false,
    			paging:true,
    			serverSide: true,
    			processing:true,
    			columnDefs: [{	                          
        	        "defaultContent": "",
        	        "targets": "_all"
        	        }],	
    			ajax:function(data,callback,settings){
    				$.ajax({
    					type:'POST',
    					url:options.url,
    					dataType: "json",
    					data:(function(){
        					var params = {};
        					/* 重构分页参数 */
        					if(data.start){
        	            		data.start = parseInt(data.start);
        	            	}
        	            	if(data.length){
        	            		data.limit = parseInt(data.length);
        	            	}
        					/* 表单参数 */
        					$.each(forms,function(findex,fobj){
        						var _params = fobj.serializeArray();
        						$.each(_params,function(i,o){
        							params[o.name]=o.value;
        						});
        					});
        					/* 扩展参数 */
        					if(settings.params){
        	            		for(var name in settings.params){
        	            			params[name] = settings.params[name];
        	            		}
        	            	}
        					return $.extend( {}, data, params );
        				})(),
        				success: function (result){
        					callback({
        						darw : data.draw,
        						recordsTotal:result[$.fn.myDatatables.defaults.mapping.ajax.recordsTotal],
        						recordsFiltered:result[$.fn.myDatatables.defaults.mapping.ajax.recordsFiltered],
        						data:result[$.fn.myDatatables.defaults.mapping.ajax.data]
        					});
        				},
        				error: function (XMLHttpRequest, textStatus, errorThrown) {
        					console.error('datatables error');
        				}
    				});
    			},
    			lengthMenu:[10, 25, 50, 100]
    		},options)).api());
    		/**
    		 * 设置选择模式
    		 */
    		var $tbody = $dom.find('tbody');
    		
    		if(options.selection){
    			if('multiple' == options.selection){
    				$tbody.on( 'click', 'tr', function () {
    			        $(this).toggleClass('selected');
    			    } );
    			}else if('single' == options.selection){
    				$tbody.on( 'click', 'tr', function () {
    					if ( $(this).hasClass('selected') ) {
    						$(this).removeClass('selected');
    					}else{
    						$tbody.find('tr.selected').removeClass('selected');
    						$(this).addClass('selected');
    					}
    				});
    			}
    		}else{
    			$tbody.on( 'click', 'tr', function () {
    				if ( $(this).hasClass('selected') ) {
    					$(this).removeClass('selected');
    				}else{
    					$tbody.find('tr.selected').removeClass('selected');
    					$(this).addClass('selected');
    				}
    			});
    		}
		},
		/**
		 * 获取多行数据
		 * @param dom
		 * @param options
		 * @returns
		 */
		getRows:function(dom,options){
			var data = $(dom).data('datatable-api').rows('.selected').data();
			if(data.length ==0){
				alert('请选择数据后操作');
				return null;
			}else{
				return data;
			}
		},
		/**
		 * 获取单行数据
		 * @param dom
		 * @param options
		 * @returns
		 */
		getRow:function(dom,options){
			var data;
			if(options != undefined && (typeof options == 'string' || typeof options == 'number')){
				data = $(dom).data('datatable-api').data();
				if(data.length>0){
					return data[options];
				}else return null;
			}else{
				data = $(dom).data('datatable-api').rows('.selected').data();
				if(data!=null){
					if(data.length == 1){
						return data[0];
					}else{
						alert('有且仅能操作一条数据！');
					}
				}else{
					alert('请选择数据后操作！');
				}
				return data;
			}
		},
		/**
		 * 刷新，保留当前页
		 * @param dom
		 * @param keepPage
		 */
		refresh:function(dom,keepPage){
			keepPage = keepPage || false;
			$(dom).data('datatable-api').draw(keepPage);
		},
		/**
		 * 重新搜索
		 * @param dom
		 * @param params
		 */
		search:function(dom,params){
			$(dom).data('datatable-api').draw();
		},
		/**
		 * 重新加载
		 */
		reload:function(dom,params){
			$(dom).data('datatable-api').draw();
		}
		/**
		 * 重置表单方法
		 * @param dom
		 * @param params,
		 
		reset:function(dom,params){
			if($dom.data('datatable-forms')){
				$.each($dom.data('datatable-forms'),function(i,o){
					o[0].reset();
					o.find('select').trigger('change');
				});
			}
		}*/
	};
	
	
})(jQuery, window, document);