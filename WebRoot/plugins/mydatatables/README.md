# MyDatatables
### 
`` 在线格式化地址 https://www.zybuluo.com/mdeditor``

------
## 版本号定义

> 软件版本号有四部分组成，第一部分为主版本号，第二部分为次版本号，第三部分为修订版本号，第四部分为日期版本号加希腊字母版本号，希腊字母版本号共有五种，分别为base、alpha、beta、RC、release
> **版本号修订：**
> 
> * 主版本号：当功能模块有较大的变动，比如增加模块或是整体架构发生变化。此版本号由项目决定是否修改。
> * 次版本号：相对于主版本号而言，次版本号的升级对应的只是局部的变动，但该局部的变动造成程序和以前版本不能兼容，或者对该程序以前的协作关系产生了破坏，或者是功能上有大的改进或增强。此版本号由项目决定是否修改。
> * 修订版本号：一般是Bug的修复或是一些小的变动或是一些功能的扩充，要经常发布修订版，修复一个严重Bug即可发布一个修订版。此版本号由项目经理决定是否修改。
> * 日期版本号：用于记录修改项目的当前日期，每天对项目的修改都需要更改日期版本号。此版本号由开发人员决定是否修改。
> * 希腊字母版本号：此版本号用于标注当前版本的软件处于哪个开发阶段，当软件进入到另一个阶段时需要修改此版本号。此版本号由项目决定是否修改。
------

## 使用方式

### 1. 对返回集合的key进行自定义
```javascript
$.fn.myDatatables.defaults.mapping.ajax = $.extend({},$.fn.myDatatables.defaults.mapping.ajax,{
    recordsTotal:'total',
	recordsFiltered:'total',
	data:'data'
});
```

### 2. 初始化插件

```javascript
$(selector).myDatatables({		
    url : 'ndasec/hire/category/queryPageList.do',
    lengthMenu : [ 10, 25, 50, 100 ],
    columns : [{
	    title : '序号',
	    render : function(data, type, row, meta) {
		    return meta.row + 1;
        }
    },
	{
		title : '名称',
		name : 'category_name',
		data : 'category_name',
	}],
	fnDrawCallback : function(setting) {
		console.log('回调方法');		
	}});
```

### 3. 获得多条记录（getRows）
返回值：若未选择则返回 null
```javascript
    var rows = $(selector).myDatatables('getRows');
```

### 4. 获得单条记录（getRow）
返回值：若未选择则返回 null
```javascript
    var row = $(selector).myDatatables('getRow');
    # 或者，选定具体行
    var row = $(selector).myDatatables('getRow',1);
```
### 5. 刷新列表（refresh）
默认保留当前页数，可选第二个参数false代表保留当前页，true代表重新刷新
```javascript
    $(selector).myDatatables('refresh',true);
```

### 6. 搜索列表（search|reload）

```javascript
    $(selector).myDatatables('search');
    # 或者
    $(selector).myDatatables('reload');
```

### 7. 重置表单[非必须方法]（reset）

```javascript
    $(selector).myDatatables('reset');
```


## 版本描述


