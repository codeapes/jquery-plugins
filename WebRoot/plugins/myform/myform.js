;(function($, window, document,undefined){
	"use strict";
	/**
	 * 
	 */
	$.fn.myForm = function(options,param){
		if (typeof options == 'string'){
			var method = $.fn.myForm.methods[options];
			if (method){
				/* 调用方法 */
				return method(this, param);
			}
		}
		options = options || {};
		return this.each(function(){
			/* 执行实例化 */
			if(!$.data(this, "datatable-api"))
				$.fn.myForm.methods.init(this,options);
		});
	}
	/**
	 * 版本信息
	 */
	$.fn.myForm.version = {
		number:'1.0.0.20170222'
	};
	/**
	 * 对外提供的方法
	 */
	$.fn.myForm.methods = {
		/**
		 * 重置表单方法
		 * @param dom
		 * @param params,
		 */
		reset:function(dom,params){
			dom[0].reset();
			dom.find('select').trigger('change');
		}
	};
	
	
})(jQuery, window, document);