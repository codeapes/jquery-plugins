# Select4
### 
`` 在线格式化地址 https://www.zybuluo.com/mdeditor``

------
## 版本号定义

> 软件版本号有四部分组成，第一部分为主版本号，第二部分为次版本号，第三部分为修订版本号，第四部分为日期版本号加希腊字母版本号，希腊字母版本号共有五种，分别为base、alpha、beta、RC、release
> **版本号修订：**
> 
> * 主版本号：当功能模块有较大的变动，比如增加模块或是整体架构发生变化。此版本号由项目决定是否修改。
> * 次版本号：相对于主版本号而言，次版本号的升级对应的只是局部的变动，但该局部的变动造成程序和以前版本不能兼容，或者对该程序以前的协作关系产生了破坏，或者是功能上有大的改进或增强。此版本号由项目决定是否修改。
> * 修订版本号：一般是Bug的修复或是一些小的变动或是一些功能的扩充，要经常发布修订版，修复一个严重Bug即可发布一个修订版。此版本号由项目经理决定是否修改。
> * 日期版本号：用于记录修改项目的当前日期，每天对项目的修改都需要更改日期版本号。此版本号由开发人员决定是否修改。
> * 希腊字母版本号：此版本号用于标注当前版本的软件处于哪个开发阶段，当软件进入到另一个阶段时需要修改此版本号。此版本号由项目决定是否修改。
------

## 使用方式

### 1. 下拉框回填（select4backfill）
html
```html
<select class="select4" name="" data-value="2">
    <option value="1">选项1</option>
    <option value="2">选项2</option>
<select>
```
javascript
```javascript
$('.select4').select4backfill();
```
### 2. ajax交互（select4ajax）
html

 - *data-url:ajax交互地址
 - *data-display-text:显示的文本，允许对象的多个值同时显示，设置方式如下：{列A}-{列B}，若单个就{列A}
 - *data-value-field:回填的值key
 - data-params:自定义提交的参数，以对象的形式填写，第一级为提交的参数key，值为一个对象（value表示需要提交的值，id表示值从某个表单组件中获取）
 - data-options:可设置默认显示的options对象，值类型为数组对象，键为具体对象返回的key
 - data-optgroup-field:用于分组的key
 - data-callback:回调方法，默认带有($_select[jquery对象],params[请求的参数],data[返回的值])

```html
<select class="select4ajax" name="select"
    data-url="ndasec/assets/host/queryList.do"
    data-display-text="{host_alias_name}"
    data-value-field="host_id"
    data-params="{param1:{value:'111'},param2:{id:'#xxx'}}"
    data-options="[{text:'选择服务器',host_id:''}]"
    data-optgroup-field="host_type"
    data-callback="callback">
</select>
```
javascript
```javascript
$select4ajax.select4ajax({
	callback:function(select,params,data){
		select.select2();
	}
});
```
## 版本描述


